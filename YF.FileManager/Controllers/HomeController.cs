﻿using QRCoder;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using YF.FileManager.Models;
using YF.FileManager.Repositories;

namespace YF.FileManager.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {

        FileRepositories f = new FileRepositories();

        public ActionResult Index()
        {

            if (User.Identity.Name == "halilyikar@neutectr.com" || User.Identity.Name == "admin@admin.com")
            {
                return View(f.GetMyFilesHalil());
            }
            else
            {
                return View(f.GetMyFilesHome(User.Identity.Name));
            }
           
        }

        public ActionResult QRCodeView(string id)
        {

            var getFile = f.GetFile(id);

            string fileURL = "http://publicfile.neutec.com.tr/Uploads/" + getFile.FileURL;


            MemoryStream ms = new MemoryStream();

            QRCodeGenerator qrGenerator = new QRCodeGenerator();
            QRCodeData qrCodeData = qrGenerator.CreateQrCode(fileURL, QRCodeGenerator.ECCLevel.Q);
            QRCode qrCode = new QRCode(qrCodeData);
            Bitmap qrCodeImage = qrCode.GetGraphic(20);

            qrCodeImage.Save(ms, ImageFormat.Png);
            ViewBag.QRCodeImage = "data:image/png;base64," + Convert.ToBase64String(ms.ToArray());



            return View(getFile);
        }



        [HttpPost]
        public ActionResult QRCodeView(HttpPostedFileBase FileUpload)
        {


            string files = "";

            if (!String.IsNullOrEmpty(Request.Form["FileName"]))
            {
                files = Request.Form["FileName"].ToString();
            }


            if (Request.Files["FileUpload"].ContentLength > 0)
            {
                string fileExtension = System.IO.Path.GetExtension(Request.Files["FileUpload"].FileName);
                string newFileName = "File_" + Guid.NewGuid().ToString() + fileExtension;
                string fileLocation = Server.MapPath("~/Uploads/") + newFileName;
                Request.Files["FileUpload"].SaveAs(fileLocation);

                int fID = f.InsertFile(newFileName, files, User.Identity.Name);


                var getFile = f.GetFileID(fID);

                string fileURL = "http://publicfile.neutec.com.tr/Uploads/" + getFile.FileURL;


                MemoryStream ms = new MemoryStream();

                QRCodeGenerator qrGenerator = new QRCodeGenerator();
                QRCodeData qrCodeData = qrGenerator.CreateQrCode(fileURL, QRCodeGenerator.ECCLevel.Q);
                QRCode qrCode = new QRCode(qrCodeData);
                Bitmap qrCodeImage = qrCode.GetGraphic(20);

                qrCodeImage.Save(ms, ImageFormat.Png);
                ViewBag.QRCodeImage = "data:image/png;base64," + Convert.ToBase64String(ms.ToArray());




            return View(getFile);
            }
            else
            {
            return Redirect("/Home");

            }

        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }




        public ActionResult MyFiles()
        {

            return View(f.GetMyFiles(User.Identity.Name));
        }

 
        public ActionResult Delete(string id)
        {
             

            f.DeleteFile(id);

            return Redirect("/Home");
        }






    }
}