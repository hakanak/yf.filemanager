﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using YF.FileManager.Repositories;

namespace YF.FileManager.Controllers
{

    [Authorize]
    public class UsersController : Controller
    {
        UserRepositories usr = new UserRepositories();


        // GET: Users
        public ActionResult Index()
        {
            return View(usr.GetUsers());
        }

        // GET: Users/Details/5
        public ActionResult Details(string id)
        {
            return View(usr.GetUser(id));
        }

        // GET: Users/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Users/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Users/Edit/5
        public ActionResult Edit(string id)
        {
            return View(usr.GetUser(id));
        }

        // POST: Users/Edit/5
        [HttpPost]
        public ActionResult Edit(string id, FormCollection collection)
        {

            var pers = usr.GetUser(id);

            string email = pers.Email;
            string username = pers.UserName;

            if (!String.IsNullOrEmpty(Request.Form["Email"]))
            {
                email = Request.Form["Email"].ToString();
            }
    

            if (!String.IsNullOrEmpty(Request.Form["UserName"]))
            {
                username = Request.Form["UserName"].ToString();
            }



            usr.UpdateUser(id, email, username);


            return RedirectToAction("Index");

        }

        // GET: Users/Delete/5
        public ActionResult Delete(string id)
        {

            usr.DeleteUser(id); 

            return Redirect("/Users");
        }

        // POST: Users/Delete/5
      
    }
}
