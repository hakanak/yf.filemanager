﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(YF.FileManager.Startup))]
namespace YF.FileManager
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
