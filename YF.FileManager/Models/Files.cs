﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace YF.FileManager.Models
{
    public class Files
    {
        public int FileID { get; set; }
        public Guid FileGuid { get; set; }
        public string FileURL { get; set; }
        public string FileName { get; set; }
        public string CreatedUser { get; set; }
        public DateTime CreatedTime { get; set; }

    }
}