﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using YF.FileManager.Models;

namespace YF.FileManager.Repositories
{
    public class FileRepositories
    {

        private SqlConnection connection = new SqlConnection(DbConnectionString.ConnectionString);


        public List<Files> GetMyFiles(string username)
        {
            return connection.Query<Files>("SELECT * FROM Files WHERE CreatedUser=@key", new { key=username }).ToList();
        }

        public List<Files> GetMyFilesHome(string username)
        {
            return connection.Query<Files>("SELECT * FROM Files WHERE CreatedUser=@key order by CreatedTime desc ", new { key = username }).ToList();
        }
        public List<Files> GetMyFilesHalil()
        {
            return connection.Query<Files>("SELECT * FROM Files  order by CreatedTime desc ").ToList();
        }
        public Files GetFile(string FileGuid)
        {
            return connection.Query<Files>("SELECT * FROM Files WHERE FileGuid=@key", new { key = FileGuid }).FirstOrDefault();
        }

        public Files GetFileID(int FileID)
        {
            return connection.Query<Files>("SELECT * FROM Files WHERE FileID=@key", new { key = FileID }).FirstOrDefault();
        }

        public bool DeleteFile(string FileGuid)
        {
            connection.Execute("DELETE FROM Files WHERE FileGuid=@key", new { key = FileGuid });

            return true;

        }

        public int InsertFile(string fileName,string name, string user)
        {
            var result = connection.Query<int>("INSERT INTO [dbo].[Files]([FileGuid] ,FileName,[FileURL] ,[CreatedUser] ,[CreatedTime]) VALUES (NEWID() ,@filename,@fileurl ,@key ,GETDATE()) SELECT CAST(SCOPE_IDENTITY() as int)", new { key = user, fileurl = fileName,filename=name });

            int resultID = result != null ? result.FirstOrDefault() : -1;
            if (resultID == -1)
                throw new Exception("Not record");


            return resultID;

        }

    }
}