﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using YF.FileManager.Models;

namespace YF.FileManager.Repositories
{
    public class UserRepositories
    {

        private SqlConnection connection = new SqlConnection(DbConnectionString.ConnectionString);


        public List<AspNetUsers> GetUsers()
        {
            return connection.Query<AspNetUsers>("SELECT * FROM AspNetUsers ").ToList();
        }

        public bool SetUserRole(string email, int role_type)
        {
            connection.Execute("UPDATE [dbo].[AspNetUsers] SET [IsAdmin] = @rol WHERE Email=@key", new { key = email, rol = role_type });

            return true;
        }



        public AspNetUsers GetUser(string userID)
        {
            return connection.Query<AspNetUsers>("SELECT * FROM AspNetUsers WHERE Id=@key", new { key = userID }).FirstOrDefault();
        }

        public Files GetFileID(int FileID)
        {
            return connection.Query<Files>("SELECT * FROM Files WHERE FileID=@key", new { key = FileID }).FirstOrDefault();
        }

        public bool DeleteFile(string FileGuid)
        {
            connection.Execute("DELETE FROM Files WHERE FileGuid=@key", new { key = FileGuid });

            return true;

        }

        public bool DeleteUser(string id)
        {
            connection.Execute("DELETE  FROM AspNetUsers WHERE Id=@key", new { key = id });

            return true;

        }

        public bool UpdateUser(string id,string mail,string username)
        {
            connection.Execute("Update  AspNetUsers set Email=@key1,UserName=@key3 WHERE Id=@key", new { key = id,key1=mail,key3=username });

            return true;

        }

        public int InsertFile(string fileName, string user)
        {
            var result = connection.Query<int>("INSERT INTO [dbo].[Files]([FileGuid] ,[FileURL] ,[CreatedUser] ,[CreatedTime]) VALUES (NEWID() ,@file ,@key ,GETDATE()) SELECT CAST(SCOPE_IDENTITY() as int)", new { key = user, file= fileName });

            int resultID = result != null ? result.FirstOrDefault() : -1;
            if (resultID == -1)
                throw new Exception("Not record");


            return resultID;

        }

    }
}